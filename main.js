document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();
    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');
    if (taskInput.value) {
        const li = document.createElement('li');
        li.innerHTML = `
            <input type="checkbox">
            <span class="mx-2">${taskInput.value}</span>
            <button class="delete-button text-red-500">Delete</button>
        `;
        taskList.appendChild(li);
        taskInput.value = '';
    }
});

document.getElementById('task-list').addEventListener('click', function(event) {
    if (event.target.matches('.delete-button')) {
        event.target.parentElement.remove();
    } else if (event.target.matches('input[type="checkbox"]')) {
        event.target.nextElementSibling.classList.toggle('line-through');
    }
});
